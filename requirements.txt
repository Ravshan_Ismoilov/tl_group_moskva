asgiref==3.5.2
Django==3.2
django-autoslug==1.9.8
psycopg2==2.9.4
pytz==2022.5
sqlparse==0.4.3
