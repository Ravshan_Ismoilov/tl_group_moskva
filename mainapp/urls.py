from django.urls import path, include
from mainapp.views import *

app_name = 'mainapp'

urlpatterns = [
    path('<slug:cat>', index, name='index'),
    path('', index, name='index')
]