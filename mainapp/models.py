from django.db import models
from django.urls import reverse
from autoslug import AutoSlugField
from django.contrib.auth import get_user_model

User = get_user_model()

STATUS_CHOICES = (
    ('active', 'Активный'),
    ('inactive', 'Неактивный'),
    ('delete', 'Удалено'))

class ActiveManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status='active')

class Category(models.Model):
	parent = models.ForeignKey('self', related_name='children', on_delete=models.CASCADE, blank=True, null=True, verbose_name="Высшая категория")
	name = models.CharField(max_length=250, unique=True, verbose_name="Название категории")
	slug = AutoSlugField(populate_from='name', unique=True, null=False, editable=False)
	description = models.TextField(verbose_name="Описание", blank=True, null=True)
	creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name="add_category", verbose_name="Админ")
	created = models.DateTimeField(auto_now_add=True, verbose_name="Дата добавления")
	updated = models.DateTimeField(auto_now=True, verbose_name="Дата изменения")
	status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='active', verbose_name="Статус")
	objects = models.Manager()
	active = ActiveManager()

	class Meta:
		ordering = ('name',)
		unique_together = ('slug', 'parent',)
		verbose_name = "Категория"
		verbose_name_plural = "Категории"

	def get_absolute_url(self):
		return reverse('mainapp:index', args=[self.slug])
	
	def get_name(self):
		return f'{self.name}'

	def __str__(self):
		full_path = [self.name]
		k = self.parent
		while k is not None:
			full_path.append(k.name)
			k = k.parent
		return ' -> '.join(full_path[::-1])

class Position(models.Model):
	title = models.CharField(max_length=250, verbose_name="Должность")
	slug = AutoSlugField(populate_from='title', unique=True, null=False, editable=False)
	creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name="add_position", verbose_name="Админ")
	created = models.DateTimeField(auto_now_add=True, verbose_name="Дата добавления")
	updated = models.DateTimeField(auto_now=True, verbose_name="Дата изменения")
	status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='active', verbose_name="Статус")
	objects = models.Manager()
	active = ActiveManager()

	class Meta:
		unique_together = ('title',)
		verbose_name = "Должность"
		verbose_name_plural = "Должности"

	def __str__(self):
		return f'{self.title}'

class Employee(models.Model):
	full_name = models.CharField(max_length=250, verbose_name="ФИО сотрудника ", db_index=True)
	position = models.ForeignKey(Position, on_delete=models.CASCADE, related_name="user_position", verbose_name="Должность")
	add_date = models.DateTimeField(verbose_name="Дата приема на работу")
	salary = models.DecimalField(max_digits=15, decimal_places=2, verbose_name="Размер заработной платы")
	category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="user_category", verbose_name="Подразделение")
	creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name="add_employee", verbose_name="Админ")
	created = models.DateTimeField(auto_now_add=True, verbose_name="Дата добавления")
	updated = models.DateTimeField(auto_now=True, verbose_name="Дата изменения")
	status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='active', verbose_name="Статус")
	objects = models.Manager()
	active = ActiveManager()

	class Meta:
		verbose_name = "Сотрудник"
		verbose_name_plural = "Сотрудники"
	
	def __str__(self):
		return f'{self.full_name}'