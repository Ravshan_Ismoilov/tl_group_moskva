from mainapp.models import Category
from django import template

register = template.Library()
# sub_category
@register.inclusion_tag('category.html')
def show_categories(cat=None):
	if cat:
		categories = Category.active.prefetch_related("user_category").filter(parent__slug=cat).all()
	else:
		categories = Category.active.prefetch_related("user_category").filter(parent=None).all()
	return {'menu_categories': categories}
