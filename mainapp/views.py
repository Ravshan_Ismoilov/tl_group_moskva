from django.shortcuts import render
from mainapp.models import Category, Position, Employee


def index(request, cat=None):
	menu_categories = Category.active.prefetch_related("user_category").filter(parent=None).all()
	categories = Category.active.prefetch_related("user_category").filter(parent__slug=cat).all()
	if cat:
		# categories = categories.filter(parent=cat).all()
		category = Category.active.prefetch_related("user_category").filter(slug=cat).first()

	return render(request, 'index.html', locals())