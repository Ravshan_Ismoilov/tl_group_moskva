from django.contrib import admin

from mainapp.models import Category, Position, Employee


# class ProductCreateInline(admin.TabularInline):
#     model = ProductCreate
#     readonly_fields = ['old_price', 'new_price', 'qty', 'creator']
#     raw_id_fields = ['product']

#     def has_add_permission(self, request, obj=None):
#     	return False

#     def has_delete_permission(self, request, obj=None):
#     	return False

class CategoryAdmin(admin.ModelAdmin):
	list_display = ('pk', 'parent', 'name', 'slug', 'creator', 'created', 'updated', 'status')
	readonly_fields = ('creator', 'created', 'updated',)
	save_on_top = True

	def save_model(self, request, obj, form, change):
		obj.creator = request.user
		super().save_model(request, obj, form, change)

class PositionAdmin(admin.ModelAdmin):
	list_display = ('pk', 'title', 'slug', 'creator', 'created', 'updated', 'status')
	readonly_fields = ('creator', 'created', 'updated',)
	save_on_top = True

	def save_model(self, request, obj, form, change):
		obj.creator = request.user
		super().save_model(request, obj, form, change)

class EmployeeAdmin(admin.ModelAdmin):
	list_display = ('pk', 'category', 'full_name', 'position', 'add_date', 'salary', 'status')
	readonly_fields = ('creator', 'created', 'updated',)
	save_on_top = True

	def save_model(self, request, obj, form, change):
		obj.creator = request.user
		super().save_model(request, obj, form, change)

admin.site.register(Category, CategoryAdmin)
admin.site.register(Position, PositionAdmin)
admin.site.register(Employee, EmployeeAdmin)